# Graphic Charter


## Primary and Secondary Logos

#### Graphic Grid

Here is the graphic grid of the logo, with the proportions to be respected at each use of the logo. Whether it is to appear in an application, on a website, on a letterhead, on a poster or in a series of logos, the proportions must ALWAYS be respected.

![](graphic_grid.png)

#### Protective Space

At all times, the logo must be surrounded by a clear area to be legible and communicate effectively. No text, lines, typographical elements, images, photographs or illustrations must occupy this space.

![](protection_space_vertical.png)
![](protection_space_horizontal.png)

#### Minimum Size

The logo should never be used when the main icon is less than 0.5 inches high.

![](minimum_size.png)

## Color Codes

#### Black

![](calimiro_black_white_background.png)
```
C0 M0 Y0 K90
R26 G26 B26
#1a1a
```
#### White

![](calimiro_white_black_background.png)
```
C0 M0 Y0 K0
R255 G255 B255
#ffffff
```
## General Uses

### Plain and dark background

To maximize the legibility of the logo on a dark, plain background, it is recommended to use the color or white version of the logo.

NEVER use the black version.

![](plain_and_dark_background.png)


### Plain and clear background

To maximize the legibility of the logo on a plain and clear background, only the color or black version of the logo is allowed.

NEVER use the white version.

![](plain_and_clear_background.png)

### Very clear background

To maximize the legibility of the logo on a very clear photo, it is recommended to use the black version of the logo.

NEVER use the white version.

![](very_clear_background.png)

## Maximum readability

To ensure maximum legibility of the logo, you must never modify or alter any part of the logo.
