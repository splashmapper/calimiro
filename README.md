![calimiro logo](docs/logo/png/calimiro_horizontal_black.png)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![pipeline status](https://gitlab.com/splashmapper/calimiro/badges/develop/pipeline.svg)](https://gitlab.com/splashmapper/calimiro/commits/develop)
[![coverage report](https://gitlab.com/splashmapper/calimiro/badges/develop/coverage.svg)](https://gitlab.com/splashmapper/calimiro/commits/develop)


Introduction
------------

Calimiro is a multi-projector auto-calibration library.

Its goal is to help with the calibration of installations involving multiple projections on arbitrary surface. Given a mean to capture images of the projections (using a camera for instance), Calimiro will:

* generate structured light patterns to be displayed onto each projections
* process the projected structured light patterns captures (using the aforementioned camera)
* estimate the intrinsic and extrinsic parameters for each projector
* generate a 3D mesh of the projection surface
* generate the texture coordinates for the said 3D mesh

### License
See [LICENSE.md](LICENSE.md).

### Authors
See [AUTHORS.md](AUTHORS.md).


Installation
------------

### Compiling on Ubuntu 20.04
Calimiro needs a compiler supporting C++17, and has been tested successfully with GCC 9 on Ubuntu 20.04.

1. Getting the project

```bash
git clone https://gitlab.com/splashmapper/calimiro
cd calimiro
```

2. Install the dependencies

```bash
sudo apt install build-essential cmake git git-lfs meshlab libopencv-dev \
libopencv-contrib-dev libgsl-dev libglm-dev libjsoncpp-dev libeigen3-dev \
graphviz libx11-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev \
mesa-common-dev libgl1-mesa-dev libglu1-mesa-dev libxxf86vm-dev libgphoto2-dev
```

3. Run the bundled dependencies build script:

```bash
./setup.sh
```

4. Build Calimiro:

```bash
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
```

Run the test to check everything behaves as expected:

```bash
cd build
make test
```

### Compilation flags

Optimization for different architectures is performed by default during compilation. It can be disabled with:
```bash
cmake -DWITH_OPTIMIZATION=OFF ..
```

Contributing
------------

See [CONTRIBUTING.md](CONTRIBUTING.md).
