/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_VOXELGRID__
#define __CALIMIRO_VOXELGRID__

#include <algorithm>
#include <limits>
#include <list>
#include <vector>

glm::vec3 maxXYZ(const std::vector<glm::vec3>& vv)
{
    glm::vec3 max_vals(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity());

    for (const auto& v : vv)
    {
        max_vals.x = std::max(max_vals.x, v.x);
        max_vals.y = std::max(max_vals.y, v.y);
        max_vals.z = std::max(max_vals.z, v.z);
    }
    return max_vals;
}

glm::vec3 minXYZ(const std::vector<glm::vec3>& vv)
{
    glm::vec3 min_vals(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());
    for (const auto& v : vv)
    {
        min_vals.x = std::min(min_vals.x, v.x);
        min_vals.y = std::min(min_vals.y, v.y);
        min_vals.z = std::min(min_vals.z, v.z);
    }
    return min_vals;
}

// Uniform grid box over 3D point
namespace calimiro
{
class Voxelgrid
{
  public:
    // Linspace creates an evenly spaced vector over a specified interval
    // inspired by https://stackoverflow.com/questions/27028226/python-linspace-in-c
    struct Linspace
    {
        double start;
        double end;
        std::size_t nb;
        std::vector<double> linspaced{};
        Linspace(double s, double e, std::size_t n)
            : start(s)
            , end(e)
            , nb(n)
        {
            if (nb == 1)
                linspaced.push_back(start);
            else if (nb > 2)
            {
                double delta = (end - start) / (nb - 1);
                for (unsigned int i = 0; i < nb - 1; ++i)
                {
                    linspaced.push_back(start + delta * i);
                }
                linspaced.push_back(end);
            }
        }
    };

    // Constructor
    Voxelgrid(std::vector<glm::vec3> p, double g)
        : _pts(p)
        , _grd_stp(g)
    {
        glm::vec3 min_xyz = minXYZ(_pts);
        glm::vec3 max_xyz = maxXYZ(_pts);

        double delta;
        double margin;
        for (unsigned int i = 0; i < 3; ++i)
        {
            delta = max_xyz[i] - min_xyz[i];
            margin = (std::floor(delta / _grd_stp) + 1) * _grd_stp - delta;
            max_xyz[i] = max_xyz[i] + (margin / 2.0);
            min_xyz[i] = min_xyz[i] - (margin / 2.0);
            _nb_vxl_xyz.push_back(static_cast<std::size_t>((max_xyz[i] - min_xyz[i]) / _grd_stp));
            _grd.emplace_back(min_xyz[i], max_xyz[i], (_nb_vxl_xyz[i] + 1));
        }
    }

    // Accessors
    double grid_step() { return _grd_stp; }
    const std::vector<glm::vec3> points() { return _pts; }
    const std::vector<Linspace> grid() { return _grd; }
    const std::vector<std::size_t> number_voxel_xyz() { return _nb_vxl_xyz; }

  private:
    const std::vector<glm::vec3> _pts;
    std::vector<Linspace> _grd;
    std::vector<std::size_t> _nb_vxl_xyz; // number of voxel along each axis; format: (n_x, n_y, n_z)
    const double _grd_stp;                // the individual voxel size along each axis
};

}; // namespace calimiro

#endif // __CALIMIRO_VOXELGRID__
