/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <memory>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/vector_angle.hpp>

#include "../calimiro/camera_model.h"
#include "../calimiro/estimator.h"
#include "../calimiro/logger.h"

using namespace calimiro;

/**
 * Verify the intrinsic and extrinsic camera parameters estimation with a camera
 * not located at the origin. The set up is based on the Blender scene in the file
 * calimiro/tests/data/check_eye.blend
 * The camera is at (7.3589, -6.9258, 4.9583), looking at point (0, 0, 0)
 */
int main()
{
    std::vector<calimiro::CalibrationPoint> pts;

    // Points and their 2D correspondence for 2 intersecting planes
    // For the 2D points, the left corner is [0, camera_resolution.y]
    pts.push_back({glm::dvec3{-9.2308, 0.38075, 0.38075}, glm::dvec2{27.0, 0.0}});
    pts.push_back({glm::dvec3{-8.4615, 0.38075, 0.38075}, glm::dvec2{83.0, 1057.0}});
    pts.push_back({glm::dvec3{-2.3077, -2.6923, 0.0}, glm::dvec2{106.0, 538.0}});
    pts.push_back({glm::dvec3{0.76923, -4.3077, 0.0}, glm::dvec2{99.0, 46.0}});
    pts.push_back({glm::dvec3{1.5385, -3.7692, 0.0}, glm::dvec2{389.0, 7.0}});
    pts.push_back({glm::dvec3{-1.5385, 1.523, 1.523}, glm::dvec2{969.0, 1049.0}});
    pts.push_back({glm::dvec3{2.3077, -2.6923, 0.0}, glm::dvec2{833.0, 44.0}});
    pts.push_back({glm::dvec3{3.0769, -2.1538, 0.0}, glm::dvec2{1139.0, 4.0}});
    pts.push_back({glm::dvec3{3.0769, -1.6154, 0.0}, glm::dvec2{1258.0, 79.0}});
    pts.push_back({glm::dvec3{2.3077, 1.9037, 1.9037}, glm::dvec2{1742.0, 964.0}});
    pts.push_back({glm::dvec3{3.8462, 0.7615, 0.7615}, glm::dvec2{1915.0, 459.0}});

    // Initialisation of the kernel
    double width = 1920.0;
    double height = 1080.0;
    Logger logger;
    cameramodel::Pinhole camera(width, height);
    Kernel kern(&logger, std::make_shared<cameramodel::Pinhole>(camera), pts);
    std::vector<int> vec_inliers;
    std::vector<double> param = kern.Ransac(vec_inliers, 0.5);

    if (param.size() == 0)
    {
        std::cout << "\ncalimiro::tests::check_eye - No camera parameters found" << std::endl;
        return 1;
    }

    // Camera has a veritcal fov of 22.90 degrees with cx = cy = 0.5
    // Verify that the estimation of the fov, cx and cy are reasonnable
    float fov_estimated = param[0];
    float cx_estimated = param[1];
    float cy_estimated = param[2];

    std::cout << "Estimated fov: " << fov_estimated << "\n"
              << "cx: " << cx_estimated << "\n"
              << "cy: " << cy_estimated << "\n";

    assert((fov_estimated > 19.0f && fov_estimated < 25.0f) == true);
    assert((cx_estimated > 0.4f && cx_estimated < 0.6f) == true);
    assert((cy_estimated > 0.4f && cy_estimated < 0.6f) == true);

    // Verify the extrinsic parameters (eye, target and up)
    int nb_param = param.size();
    glm::dvec3 estimated_eye{param[nb_param - 6], param[nb_param - 5], param[nb_param - 4]};
    glm::dvec3 estimated_euler{param[nb_param - 3], param[nb_param - 2], param[nb_param - 1]};
    Camera::Extrinsics estimated_extrinsics = camera.computeExtrinsics(estimated_eye, estimated_euler);

    // Verify eye
    glm::dvec3 true_eye{7.3589, -6.9258, 4.9583};
    std::cout << "Eye: " << estimated_extrinsics.eye[0] << " " << estimated_extrinsics.eye[1] << " " << estimated_extrinsics.eye[2] << "\n";
    assert((glm::distance(estimated_extrinsics.eye, true_eye) < 0.25) == true);

    // Verify target
    // Target is a position -> there is multiple solutions along a line
    // To verify the target estimation, we make it a direction from the origin
    glm::dvec3 true_target_position{0.0, 0.0, 0.0};
    glm::dvec3 true_target_dir = glm::normalize(true_target_position - true_eye);
    glm::dvec3 estimated_target_direction = glm::normalize(glm::dvec3(estimated_extrinsics.target - estimated_extrinsics.eye));
    std::cout << "Target direction: " << estimated_target_direction[0] << " " << estimated_target_direction[1] << " " << estimated_target_direction[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_target_direction, true_target_dir, glm::cross(estimated_target_direction, true_target_dir))) < 0.05) == true);

    // Verify up
    glm::dvec3 z_axis{0.0, 0.0, 1.0};
    glm::dvec3 vec_on_plane = glm::cross(z_axis, true_target_dir);
    glm::dvec3 true_up = glm::normalize(glm::cross(true_target_dir, vec_on_plane));

    std::cout << "Up: " << estimated_extrinsics.up[0] << " " << estimated_extrinsics.up[1] << " " << estimated_extrinsics.up[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_extrinsics.up, true_up, glm::cross(estimated_extrinsics.up, true_up))) < 0.05) == true);

    return 0;
}
