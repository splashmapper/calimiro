/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_GEOMETRY__
#define __CALIMIRO_GEOMETRY__

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "./calimiro/abstract_logger.h"
#include "./calimiro/texture_coordinates/texCoordUtils.h"

namespace calimiro
{
class Geometry
{
  public:
    struct FaceVertex
    {
        int vertexID{-1};
        int normalID{-1};
        int uvID{-1};
    };
    struct SphereShape
    {
        glm::vec3 center;
        double radius;
    };

    // Constructors
    Geometry(AbstractLogger* log)
        : _log(log){};
    Geometry(AbstractLogger* log,
        const std::vector<glm::vec3>& vertices,
        const std::vector<glm::vec3>& normals,
        const std::vector<glm::vec2>& uvs,
        const std::vector<std::vector<FaceVertex>>& faces);

    // Accesssors
    std::size_t vertices_size() const { return _nb_vertices; };
    std::size_t faces_size() const { return _faces.size(); };

    const std::vector<glm::vec3>& vertices() const { return _vertices; };
    const std::vector<glm::vec3>& normals() const { return _normals; };
    const std::vector<glm::vec2>& uvs() const { return _uvs; };
    const std::vector<std::vector<FaceVertex>>& faces() const { return _faces; };

    // Returns a geometry with the normals computed from the points
    // Currently this function is dependent on Meshlab
    Geometry computeNormalsPointSet(unsigned int nb_neighbors = 10);

    // Returns a filtered geometry where the outliers points are removed
    // If the geometry had faces, the faces are lost
    // threshold: a point is an outlier if the standard deviation from the mean of the average distance to its nearest neighbors is > threshold
    // nb_neighbors: number of nearest neighbors to a point. Smaller values makes it filter more sensitve to noise, increasing it increases the number of computations
    Geometry denoise(double threshold = 0.1, int nb_neighbors = 12);

    // Returns a downsampled geometry
    // If the geometry had faces, the faces are lost
    // grid_step: the grid_step specifies the grid size of the 3D bounding box
    Geometry downsample(double grid_step = 0.1);

    // Returns the center and radius (as a struct) of the points from a geometry
    SphereShape leastSquareSphereFit();

    // Returns a mesh built with the marching cubes algorithm
    // The geometry must have its normals defined
    Geometry marchingCubes(unsigned int resolution = 200);

    // Returns a normalized and centered sphere geometry
    // sphereShape: struct describing the sphere obtained from leastSquareSphereFit();
    Geometry normalizeSphere(const SphereShape& sphereShape);

    /**
     * Compute a set of texture coordinates for the geometry.
     * \param generator the generator to compute the texture coordinates with.
     */
    Geometry computeTextureCoordinates(TexCoordGen* generator);

    // Returns a simplified geometry with instant-meshes https://github.com/wjakob/instant-meshes
    // rosy: specifies the orientation symmetry type (2, 4, or 6)
    // posy: specifies the position symmetry type (3 or 4)
    // face_count: target face count of the output mesh
    // vertex_count: target vertex count of the output mesh
    // scale: target world space length of edges in the output
    Geometry simplifyGeometry(int rosy = 6, int posy = 3, int face_count = -1, int vertex_count = -1, float scale = -1);

    // Returns a simplified geometry with pmp-lirary
    // target_percentage: speficies the percentage of number of vertices of the target mesh
    //                    with respect to the original geometry
    Geometry simplifyGeometryPMP(int target_percentage = 10);

  protected:
    AbstractLogger* _log;
    std::vector<glm::vec3> _vertices{};
    std::vector<glm::vec3> _normals{};
    std::vector<glm::vec2> _uvs{};
    std::vector<std::vector<FaceVertex>> _faces{};
    std::size_t _nb_vertices{0};

    // Generates the script compute normals from a point set to be provided to Meshlab
    bool generateComputeNormalsScript(const std::string& filename, unsigned int nb_neighbors);
    // Generates the script for the marching cubes algorithm
    bool generateMarchingCubesScript(const std::string& filename, unsigned int resolution);
};

}; // namespace calimiro

#endif // __CALIMIRO_GEOMETRY__
