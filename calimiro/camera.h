/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_CAMERA__
#define __CALIMIRO_CAMERA__

#include <vector>

#include <glm/glm.hpp>

namespace calimiro
{

class Camera
{
  public:
    struct Intrinsics
    {
        Intrinsics(){};
        Intrinsics(double fovV, double widthV, double heightV, double nearV, double farV, double cxV, double cyV)
            : fov(fovV)
            , width(widthV)
            , height(heightV)
            , near(nearV)
            , far(farV)
            , cx(cxV)
            , cy(cyV){};

        double fov{45.0};                     // Vertical FOV in degrees
        double width{1920.0}, height{1080.0}; // Current width and height
        double near{0.1}, far{1000.0};        // Near and far parameters
        double cx{0.5}, cy{0.5};              // Relative position of the lens center; principal points
    };

    struct Extrinsics
    {
        Extrinsics(){};
        Extrinsics(glm::dvec3 e, glm::dvec3 t, glm::dvec3 u)
            : eye(e)
            , target(t)
            , up(u){};

        glm::dvec3 eye{0.0, 0.0, 0.0};    // Camera position
        glm::dvec3 target{0.0, 1.0, 0.0}; // Camera target
                                          // The target is defined as a position to be
                                          // consistent with the creation of a lookAt matrix
                                          // It is not a normalized direction.
        glm::dvec3 up{0.0, 0.0, 1.0};     // Camera up vector
    };

    struct Transforms
    {
        Transforms(glm::dmat4 look, glm::dmat4 proj, glm::dvec4 viewport)
            : lookM(look)
            , projM(proj)
            , viewportV(viewport)
        {
        }
        glm::dmat4 lookM;
        glm::dmat4 projM;
        glm::dvec4 viewportV;
    };

    Camera(){};
    Camera(double w = 0.0, double h = 0.0, double vertical_fov = 0.0, double cx = 0.0, double cy = 0.0, double near = 0.1, double far = 1000.0)
        : _intrinsics(vertical_fov, w, h, near, far, cx, cy){};

    /**
     * Convert screen coordinates to NDC coordinates (ie [-1,1])
     * Normalize over the max resolution and recenter the frame of reference
     * lower left corner: [0,0] -> [-1 * aspect ratio * width, -1 * aspect ratio * height]
     * Origin is now centered around the optical center, no longer the lower left corner
     * \param glm::vector 2d screen coordinates, width, height, principal points of camera
     * \return Return a 2d glm::vector in NDC coordinates
     */
    static glm::dvec2 toNDC(glm::dvec2 screen, double width = 1920.0, double height = 1080.0, double cx = 0.5, double cy = 0.5);

    /**
     * Convert NDC coordinates back to screen coordinates according to the resolution
     * \param glm::vector 2d coordinates in the NDC frame of reference, width, height, principal points of camera
     * \return Return a 2d glm::vector in screen coordinates
     */
    static glm::dvec2 toScreen(glm::dvec2 normalized, double width = 1920.0, double height = 1080.0, double cx = 0.5, double cy = 0.5);
    static Extrinsics computeExtrinsics(const glm::dvec3 eye, const glm::dvec3 euler);
    static glm::dmat4 computeProjectionMatrix(const Intrinsics& intrinsics);
    static Transforms computeTransformationMatrices(const Extrinsics& extrinsics, const Intrinsics& intrinsics);
    static glm::dvec2 computeProjection(const Transforms& transform, const glm::dvec3& world_pt);
    Intrinsics intrinsics() const { return _intrinsics; };
    Extrinsics extrinsics() const { return _extrinsics; };

    /**
     * Transforms a point from the screen plane to the camera plane (ie [-1,1])
     * \param glm::vector 2d screen coordinates
     * \return Return a 2d glm::vector in NDC coordinates
     */
    glm::dvec2 ima2cam(const glm::dvec2& p) const;

    /**
     * Transforms a point from the camera plane to the screen/image plane
     * \param glm::vector 2d camera coordinates
     * \return Return a 2d glm::vector in the image plane
     */
    glm::dvec2 cam2ima(const glm::dvec2& p) const;

    /**
     * \return Return a std::vector of the estimated intrinsic parameters
     */
    virtual std::vector<double> getParams() const = 0;

    /**
     * Add the distortion field to a point in the camera plane (NDC coordinates)
     * \param glm::vector 2d camera coordinates ([-1, 1])
     * \return Return a point with distortion in the camera plane
     */
    virtual glm::dvec2 addDisto(const glm::dvec2& undistorted) const = 0;

    /**
     * Add the distortion field to a point in the screen/image plane
     * \param glm::vector 2d screen coordinates
     * \return Return a point with distortion in the image plane
     */
    virtual glm::dvec2 getDistortedPixel(const glm::dvec2& pixel) const = 0;

    /**
     * Update the intrinsics of the camera
     * \param params vector of parameters to update the intrinsics
     */
    virtual void updateFromParams(const std::vector<double>& params) = 0;

    /**
     * Update the extrinsics of the camera
     * \param glm::dvec3 representing the eye (camera position), glm::dvec3 euleur angles
     */
    void updateExtrinsics(glm::dvec3 eye, glm::dvec3 euler) { _extrinsics = computeExtrinsics(eye, euler); }
    static int const MINIMUM_SAMPLES = 6;

  protected:
    Intrinsics _intrinsics;
    Extrinsics _extrinsics{};
};

};     // namespace calimiro
#endif // __CALIMIRO_CAMERA__
