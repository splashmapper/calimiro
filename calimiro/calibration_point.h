/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CALIMIRO_CALIBRATION_POINT__
#define __CALIMIRO_CALIBRATION_POINT__

#include <glm/gtc/matrix_transform.hpp>

namespace calimiro
{

struct CalibrationPoint
{
    CalibrationPoint() {}
    explicit CalibrationPoint(glm::dvec3 w)
    {
        world = w;
        screen = glm::dvec2(0.f, 0.f);
    }
    CalibrationPoint(glm::dvec3 w, glm::dvec2 s)
    {
        world = w;
        screen = s;
    }
    glm::dvec3 world;
    glm::dvec2 screen;
    float weight{1.f};
};

} // namespace calimiro

#endif // __CALIMIRO_CALIBRATION_POINT__
