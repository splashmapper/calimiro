/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_CAMERA_MODEL__
#define __CALIMIRO_CAMERA_MODEL__

#include <vector>

#include <glm/glm.hpp>

#include "./camera.h"

namespace calimiro
{
namespace cameramodel
{
/**
 * Implements an ideal pinhole camera (skew and distortion are not taken into account)
 */

class Pinhole : public Camera
{
  public:
    static int const MINIMUM_SAMPLES = 7;
    Pinhole(double w = 0.0, double h = 0.0, double vertical_fov = 0.0, double cx = 0.0, double cy = 0.0, double near = 0.1, double far = 1000.0)
        : Camera(w, h, vertical_fov, cx, cy, near, far){};

    /**
     * \return Return a std::vector of the estimated intrinsic parameters: fov, cx, cy
     */
    std::vector<double> getParams() const override { return {_intrinsics.fov, _intrinsics.cx, _intrinsics.cy}; };

    /**
     * Add the distortion field to a point in the camera plane (NDC coordinates)
     * \param glm::vector 2d camera coordinates ([-1, 1])
     * \return Return a point with distortion in the camera plane
     */
    glm::dvec2 addDisto(const glm::dvec2& undistorted) const override { return undistorted; };

    /**
     * Add the distortion field to a point in the screen/image plane
     * \param glm::vector 2d screen coordinates
     * \return Return a point with distortion in the image plane
     */
    glm::dvec2 getDistortedPixel(const glm::dvec2& pixel) const override { return pixel; };

    /**
     * Update the intrinsics of the camera
     * \param params vector of parameters to update the intrinsics
     */
    void updateFromParams(const std::vector<double>& params) override;
};

/**
 * Defines a pinhole camera with one radial coefficient of distortion according to Brown's model
 */
class Pinhole_Radial_k1 : public Pinhole
{
  protected:
    std::vector<double> _params;

  public:
    static int const MINIMUM_SAMPLES = 8;
    Pinhole_Radial_k1(double w = 0.0, double h = 0.0, double vertical_fov = 0.0, double cx = 0.0, double cy = 0.0, double k1 = 0.0, double near = 0.1, double far = 1000.0)
        : Pinhole(w, h, vertical_fov, cx, cy, near, far)
        , _params({k1}){};

    std::vector<double> getParams() const override;

    glm::dvec2 addDisto(const glm::dvec2& p) const override;

    glm::dvec2 getDistortedPixel(const glm::dvec2& pixel) const override;

    void updateFromParams(const std::vector<double>& params) override;
};

/**
 * Defines a pinhole camera with three radial coefficients of distortion according to Brown's model
 */
class Pinhole_Radial_k3 : public Pinhole
{
  protected:
    std::vector<double> _params;

  public:
    static int const MINIMUM_SAMPLES = 10;
    Pinhole_Radial_k3(double w = 0.0,
        double h = 0.0,
        double vertical_fov = 0.0,
        double cx = 0.0,
        double cy = 0.0,
        double k1 = 0.0,
        double k2 = 0.0,
        double k3 = 0.0,
        double near = 0.1,
        double far = 1000.0)
        : Pinhole(w, h, vertical_fov, cx, cy, near, far)
        , _params({k1, k2, k3}){};

    std::vector<double> getParams() const override;

    glm::dvec2 addDisto(const glm::dvec2& p) const override;

    glm::dvec2 getDistortedPixel(const glm::dvec2& pixel) const override;

    void updateFromParams(const std::vector<double>& params) override;
};

} // namespace cameramodel
} // namespace calimiro
#endif // __CALIMIRO_CAMERA_MODEL__
