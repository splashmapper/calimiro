/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./reconstruction.h"

#include <cassert>
#include <cereal/archives/json.hpp>
#include <cereal/cereal.hpp>
#include <cereal/types/polymorphic.hpp>

#include <openMVG/cameras/cameras.hpp> // This needs to be separated from the rest, because of the linter

#include <openMVG/cameras/Cameras_Common_command_line_helper.hpp>
#include <openMVG/exif/sensor_width_database/ParseDatabase.hpp>
#include <openMVG/features/regions.hpp>
#include <openMVG/graph/graph.hpp>
#include <openMVG/matching/indMatch.hpp>
#include <openMVG/matching/indMatch_utils.hpp>
#include <openMVG/matching/matcher_type.hpp>
#include <openMVG/matching/pairwiseAdjacencyDisplay.hpp>
#include <openMVG/matching_image_collection/F_ACRobust.hpp>
#include <openMVG/matching_image_collection/GeometricFilter.hpp>
#include <openMVG/matching_image_collection/Matcher.hpp>
#include <openMVG/matching_image_collection/Matcher_Regions.hpp>
#include <openMVG/matching_image_collection/Pair_Builder.hpp>
#include <openMVG/sfm/pipelines/sequential/sequential_SfM.hpp>
#include <openMVG/sfm/pipelines/sfm_features_provider.hpp>
#include <openMVG/sfm/pipelines/sfm_matches_provider.hpp>
#include <openMVG/sfm/pipelines/sfm_regions_provider.hpp>
#include <openMVG/sfm/pipelines/structure_from_known_poses/structure_estimator.hpp>
#include <openMVG/sfm/sfm_data.hpp>
#include <openMVG/sfm/sfm_data_filters.hpp>
#include <openMVG/sfm/sfm_data_io.hpp>
#include <openMVG/sfm/sfm_data_io_ply.hpp>
#include <openMVG/sfm/sfm_data_utils.hpp>
#include <openMVG/sfm/sfm_report.hpp>
#include <openMVG/sfm/sfm_view.hpp>
#include <openMVG/stl/stl.hpp>
#include <openMVG/system/timer.hpp>
#include <openMVG/types.hpp>
#include <opencv2/opencv.hpp>

#include "./constants.h"
#include "./image_describer.h"

namespace calimiro
{

openMVG::cameras::EINTRINSIC cameraModelToMVG(Reconstruction::CameraModel model)
{
    switch (model)
    {
    default:
        break;
    case Reconstruction::CameraModel::PINHOLE_CAMERA:
        return openMVG::cameras::PINHOLE_CAMERA;
    case Reconstruction::CameraModel::PINHOLE_CAMERA_RADIAL1:
        return openMVG::cameras::PINHOLE_CAMERA_RADIAL1;
    case Reconstruction::CameraModel::PINHOLE_CAMERA_RADIAL3:
        return openMVG::cameras::PINHOLE_CAMERA_RADIAL3;
    case Reconstruction::CameraModel::PINHOLE_CAMERA_BROWN:
        return openMVG::cameras::PINHOLE_CAMERA_BROWN;
    case Reconstruction::CameraModel::PINHOLE_CAMERA_FISHEYE:
        return openMVG::cameras::PINHOLE_CAMERA_FISHEYE;
    case Reconstruction::CameraModel::CAMERA_SPHERICAL:
        return openMVG::cameras::CAMERA_SPHERICAL;
    }

    assert(false);
    return openMVG::cameras::PINHOLE_CAMERA;
}

class Reconstruction::Impl
{
  private:
    AbstractLogger* _log;
    openMVG::sfm::SfM_Data _sfm_data;
    std::filesystem::path _workspace;
    CameraModel _i_User_camera_model;
    bool write_details;
    std::filesystem::path _sFeaturesDir;
    std::filesystem::path _sMatchesDir;
    std::filesystem::path _sOutputDir;
    std::filesystem::path _sSfM_Data_Filename;

  public:
    Impl(AbstractLogger* log, std::filesystem::path p = utils::getWorkspace(), CameraModel camera_model = CameraModel::PINHOLE_CAMERA_RADIAL3, bool write_report = false)
        : _log(log)
        , _workspace(p)
        , _i_User_camera_model(camera_model)
        , write_details(write_report)
        , _sFeaturesDir(_workspace / constants::cFeatureDirectory)
        , _sMatchesDir(_workspace / constants::cMatchesDirectory)
        , _sOutputDir(_workspace / constants::cOutputDirectory)
        , _sSfM_Data_Filename(_sOutputDir / constants::cSfM_data_filename)
    {
        std::filesystem::create_directory(_workspace);
    };

    void sfmInitImageListing(double focal_pixels)
    {
        // Define some variables to a default values, will be ajusted as we go along
        const std::filesystem::path sImageDir = _workspace / "decoded_matrix";
        const std::filesystem::path sfileDatabase(std::string(CMAKE_INSTALL_PREFIX) + constants::cDatabase_path);

        _log->message("ReconstructionImpl::sfmInitImageListing with the following parameters:"
                      "\n    --imageDirectory %"
                      "\n    --sensorWidthDatabase %"
                      "\n    --focal %"
                      "\n    --camera_model %",
            sImageDir,
            sfileDatabase,
            std::to_string(focal_pixels),
            std::to_string(_i_User_camera_model));

        // Properties of the images
        double width = -1, height = -1, focal = -1, ppx = -1, ppy = -1;
        const auto e_User_camera_model = cameraModelToMVG(_i_User_camera_model);

        if (!std::filesystem::is_directory(sImageDir))
        {
            _log->error("ReconstructionImpl::sfmInitImageListing: The input directory: % doesn't exist", sImageDir);
            return;
        }

        std::filesystem::create_directory(_sOutputDir);

        std::vector<Datasheet> vec_database;
        if (!sfileDatabase.empty())
        {
            if (!parseDatabase(sfileDatabase, vec_database))
            {
                _log->error("ReconstructionImpl::sfmInitImageListing: Invalid input database: %"
                            ", please specify a valid file",
                    sfileDatabase);
                return;
            }
        }

        std::vector<std::filesystem::path> vec_image;
        for (const auto& file : std::filesystem::directory_iterator(sImageDir))
        {
            vec_image.push_back(file.path()); // here I assume I will only have the yml files in that folder
        }
        std::sort(vec_image.begin(), vec_image.end());

        _sfm_data.s_root_path = sImageDir; // Setup image root_path
        openMVG::sfm::Views& views = _sfm_data.views;
        openMVG::sfm::Intrinsics& intrinsics = _sfm_data.intrinsics;

        cv::FileStorage fs;
        cv::FileStorage fs_meta;
        for (const auto& path : vec_image)
        {
            // Read the file to fill (w, h, focal, ppx, ppy) fields
            width = height = ppx = ppy = focal = -1.0;
            const std::string sImageFilename = path.stem();
            const std::string sImFilenamePart = path.extension();

            // Make sure the format is .yml
            if (sImFilenamePart != ".yml")
            {
                _log->warning("%: Unkown image file format", sImFilenamePart);
                continue; // cannot open yaml file, go to the next one
            }

            // get the width and height from the metadata file if available
            // we prioritize the metadata file due to the large size of the yml image file
            if (std::filesystem::exists(_workspace / "metadata.yml"))
            {
                fs_meta.open((_workspace / "metadata.yml").string(), cv::FileStorage::READ);
                width = fs_meta["image_width"];
                height = fs_meta["image_height"];
                ppx = width / 2.0;
                ppy = height / 2.0;
                fs_meta.release();
            }
            else
            {
                fs.open(path.string(), cv::FileStorage::READ);
                width = fs[sImageFilename]["cols"];
                height = fs[sImageFilename]["rows"];
                ppx = width / 2.0;
                ppy = height / 2.0;
                fs.release();
            }

            if (focal_pixels != -1)
                focal = focal_pixels;

            // If focal not provided or wrongly provided
            if (focal == -1)
            {
                // Check that there is exif data
                if (std::filesystem::exists(_workspace / "metadata.yml"))
                {
                    fs_meta.open((_workspace / "metadata.yml").string(), cv::FileStorage::READ);
                    float focal_length = fs_meta["focal_length"];
                    const std::string sCamModel = fs_meta["camera_model"];
                    if (focal_length <= 0.0f)
                    {
                        _log->warning("%: Focal length is missing", sImageFilename);
                        focal = -1.0;
                    }
                    else // use the value in the database
                    {
                        Datasheet datasheet;
                        if (getInfo(sCamModel, vec_database, datasheet))
                        {
                            // The camera model was found in the database so we can compute it's approximated focal length
                            const double sensor_size = datasheet.sensorSize_;
                            focal = std::max(width, height) * focal_length / sensor_size;
                        }
                        else
                        {
                            _log->warning("% : model \"%\"doesn't exist in the database", sImageFilename, sCamModel);
                        }
                        fs_meta.release();
                    }
                }
            }
            // Build intrinsic parameter related to the view
            std::shared_ptr<openMVG::cameras::IntrinsicBase> intrinsic = nullptr;
            if (focal > 0 && ppx > 0 && ppy > 0 && width > 0 && height > 0)
            {
                // Create the desired camera type
                switch (e_User_camera_model)
                {
                case openMVG::cameras::PINHOLE_CAMERA:
                    intrinsic = std::make_shared<openMVG::cameras::Pinhole_Intrinsic>(width, height, focal, ppx, ppy);
                    break;
                case openMVG::cameras::PINHOLE_CAMERA_RADIAL1:
                    intrinsic = std::make_shared<openMVG::cameras::Pinhole_Intrinsic_Radial_K1>(width, height, focal, ppx, ppy, 0.0); // setup no distortion as initial guess
                    break;
                case openMVG::cameras::PINHOLE_CAMERA_RADIAL3:
                    intrinsic =
                        std::make_shared<openMVG::cameras::Pinhole_Intrinsic_Radial_K3>(width, height, focal, ppx, ppy, 0.0, 0.0, 0.0); // setup no distortion as initial guess
                    break;
                case openMVG::cameras::PINHOLE_CAMERA_BROWN:
                    intrinsic = std::make_shared<openMVG::cameras::Pinhole_Intrinsic_Brown_T2>(
                        width, height, focal, ppx, ppy, 0.0, 0.0, 0.0, 0.0, 0.0); // setup no distortion as initial guess
                    break;
                case openMVG::cameras::PINHOLE_CAMERA_FISHEYE:
                    intrinsic =
                        std::make_shared<openMVG::cameras::Pinhole_Intrinsic_Fisheye>(width, height, focal, ppx, ppy, 0.0, 0.0, 0.0, 0.0); // setup no distortion as initial guess
                    break;
                case openMVG::cameras::CAMERA_SPHERICAL:
                    intrinsic = std::make_shared<openMVG::cameras::Intrinsic_Spherical>(width, height);
                    break;
                default:
                    _log->error("ReconstructionImpl::sfmInitImageListing: "
                                "Error: unknown camera model: ",
                        std::to_string(static_cast<int>(e_User_camera_model)));
                    return;
                }
            }
            // Build the view corresponding to the image
            auto v = std::make_shared<openMVG::sfm::View>(path.filename(), views.size(), views.size(), views.size(), width, height);
            // Add intrinsic related to the image (if any)
            if (!intrinsic)
            {
                // Since the view have invalid intrinsic data
                // (export the view, with an invalid intrinsic field value)
                v->id_intrinsic = openMVG::UndefinedIndexT;
            }
            else
            {
                // Add the defined intrinsic to the sfm_container
                intrinsics[v->id_intrinsic] = intrinsic;
            }
            // Add the view to the sfm_container
            views[v->id_view] = v;
        }

        // Group camera that share common properties (leads to more faster & stable BA).
        openMVG::sfm::GroupSharedIntrinsics(_sfm_data);

        // Store SfM_Data views & intrinsic data
        if (!openMVG::sfm::Save(_sfm_data, _sSfM_Data_Filename.string(), openMVG::sfm::ESfM_Data(openMVG::sfm::VIEWS | openMVG::sfm::INTRINSICS)))
        {
            _log->error("ReconstructionImpl::sfmInitImageListing: "
                        "Could not save SfM data to file \"%\"",
                _sSfM_Data_Filename.string());
            return;
        }
        _log->message("SfMInit_ImageListing report:"
                      "\n    listed #File(s): %"
                      "\n    usable #File(s) listed in sfm_data: %"
                      "\n    usable #Intrinsic(s) listed in sfm_data: %",
            std::to_string(vec_image.size()),
            std::to_string(_sfm_data.GetViews().size()),
            std::to_string(_sfm_data.GetIntrinsics().size()));
    };

    void computeFeatures()
    {
        _log->message("ReconstructionImpl::computeFeatures with the following parameters:"
                      "\n    --input file %"
                      "\n    --features output directory %",
            _sSfM_Data_Filename,
            _sFeaturesDir);

        // verify features directory
        if (!std::filesystem::create_directory(_sFeaturesDir))
        {
            _log->warning("ReconstructionImpl::computeFeatures: "
                          "Directory \"%\" could not be created",
                _sFeaturesDir);
        }

        std::unique_ptr<STRUCTURED_LIGHT_Image_describer> image_describer;

        if (std::filesystem::exists(_sOutputDir / constants::cImage_describer))
        {
            // Dynamically load the image_describer from the file (restore old settings)
            std::ifstream stream(_sOutputDir / constants::cImage_describer);
            if (!stream.is_open())
            {
                _log->error("ReconstructionImpl::computeFeatures: "
                            "Invalid \"%\" file",
                    _sOutputDir / constants::cImage_describer);
                return;
            }
            cereal::JSONInputArchive archive(stream);
            archive(cereal::make_nvp("image_describer", image_describer));
        }
        else
        {
            image_describer = std::make_unique<STRUCTURED_LIGHT_Image_describer>();

            // Export the image_describer and region type
            {
                std::ofstream stream(_sOutputDir / constants::cImage_describer);
                if (!stream.is_open())
                    return;
                cereal::JSONOutputArchive archive(stream);
                archive(cereal::make_nvp("image_describer", image_describer));
                auto regions = image_describer->Allocate();
                archive(cereal::make_nvp("regions_type", regions));
            }
        }

        // Feature extraction routine:
        // For each View of the SfM_Data container:
        // - if regions file exist continue,
        // - if no file, compute features
        openMVG::system::Timer timer;

        for (const auto& view_it : _sfm_data.GetViews())
        {
            const openMVG::sfm::View* view = view_it.second.get();
            const std::filesystem::path sView_filename = std::filesystem::path(_sfm_data.s_root_path) / view->s_Img_path;
            const std::filesystem::path sFeat = (_sFeaturesDir / sView_filename.stem()).replace_extension(".feat");
            const std::filesystem::path sDesc = (_sFeaturesDir / sView_filename.stem()).replace_extension(".desc");

            // if features or descriptors file are missing, compute them
            if (!std::filesystem::exists(sFeat) || !std::filesystem::exists(sDesc))
            {
                auto regions = image_describer->Describe(sView_filename);
                if (regions && !image_describer->Save(regions.get(), sFeat, sDesc))
                {
                    _log->error("ReconstructionImpl::computeFeatures: "
                                "Cannot save regions for image \"%\""
                                "\nStopping feature extraction",
                        sView_filename);
                    break;
                }
            }
        }
        _log->message("Task done in (s): ", std::to_string(timer.elapsed()));
    };

    void computeMatches()
    {
        std::string sGeometricModel = "f";
        float fDistRatio = 0.8f;
        std::string sNearestMatchingMethod = "ANNL2";
        int imax_iteration = 2048;

        _log->message("ReconstructionImpl::computeMatches with the following parameters:"
                      "\n   --input_file %"
                      "\n   --matches output directory %"
                      "\n   --nearest_matching_method %"
                      "\n   --ratio %",
            _sSfM_Data_Filename,
            _sMatchesDir,
            sNearestMatchingMethod,
            std::to_string(fDistRatio));

        std::string sGeometricMatchesFilename = "matches.f.bin";

        if (!std::filesystem::create_directory(_sMatchesDir))
        {
            _log->warning("ReconstructionImpl::computeMatches: "
                          "Directory \"%\" could not be created",
                _sMatchesDir);
        }

        // load SfM scene regions
        std::unique_ptr<openMVG::features::Regions> regions_type = openMVG::features::Init_region_type_from_file(_sOutputDir / constants::cImage_describer);
        if (!regions_type)
        {
            _log->error("ReconstructionImpl::computeMatches: "
                        "Invalid \"%\" regions type file",
                _sOutputDir / constants::cImage_describer);
            return;
        }

        // compute inital matches
        auto regions_provider = std::make_shared<openMVG::sfm::Regions_Provider>();
        if (!regions_provider->load(_sfm_data, _sFeaturesDir, regions_type))
        {
            _log->error("ReconstructionImpl::computeMatches: Invalid regions");
            return;
        }

        openMVG::matching::PairWiseMatches map_PutativesMatches;

        std::vector<std::string> vec_fileNames;
        std::vector<std::pair<std::size_t, std::size_t>> vec_imagesSize;
        vec_fileNames.reserve(_sfm_data.GetViews().size());
        vec_imagesSize.reserve(_sfm_data.GetViews().size());

        for (const auto& view_it : _sfm_data.GetViews())
        {
            const openMVG::sfm::View* v = view_it.second.get();
            vec_fileNames.push_back(std::filesystem::path(_sfm_data.s_root_path) / v->s_Img_path);
            vec_imagesSize.push_back(std::make_pair(v->ui_width, v->ui_height));
        }

        // load the matches if they already exists
        if (std::filesystem::exists(_sMatchesDir / constants::cMatchesPutativeOutput_bin) || std::filesystem::exists(_sMatchesDir / constants::cMatchesPutativeOutput_txt))
        {
            if (!openMVG::matching::Load(map_PutativesMatches, _sMatchesDir / constants::cMatchesPutativeOutput_bin) &&
                !openMVG::matching::Load(map_PutativesMatches, _sMatchesDir / constants::cMatchesPutativeOutput_txt))
            {
                _log->error("ReconstructionImpl::computeMatches: Cannot load input matches file");
                return;
            }
            _log->message("PREVIOUS RESULTS LOADED; #pair: %", std::to_string(map_PutativesMatches.size()));
        }
        else // compute the matches
        {
            // Allocate the ANN_L2 matcher
            auto collectionMatcher = std::make_unique<openMVG::matching_image_collection::Matcher_Regions>(fDistRatio, openMVG::matching::EMatcherType::ANN_L2);
            if (!collectionMatcher)
            {
                _log->error("ReconstructionImpl::computeMatches: "
                            "Invalid Nearest Neighbor method: ANN_L2");
                return;
            }
            _log->message("Using ANN_L2 matcher");

            // perform the matching
            openMVG::system::Timer timer;
            auto pairs = openMVG::exhaustivePairs(_sfm_data.GetViews().size());

            // photometric matching of putative pairs
            collectionMatcher->Match(regions_provider, pairs, map_PutativesMatches, nullptr);

            // export matches
            if (!openMVG::matching::Save(map_PutativesMatches, _sMatchesDir / constants::cMatchesPutativeOutput_bin))
            {
                _log->error("ReconstructionImpl::computeMatches: "
                            "Cannot save computed matches in \"$\"",
                    _sMatchesDir / "matches.putative.bin");
                return;
            }
            _log->message("Task (Regions Matching) done in (s): ", std::to_string(timer.elapsed()));
        }

        if (write_details)
        {
            // export initial matches Adjacency matrix
            openMVG::matching::PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(), map_PutativesMatches, _sMatchesDir / constants::cPutativeAdjacencyMatrix_svg);

            // export view pair graph once initial graph matches has been computed
            std::set<openMVG::IndexT> set_ViewIds;
            std::transform(_sfm_data.GetViews().begin(), _sfm_data.GetViews().end(), std::inserter(set_ViewIds, set_ViewIds.begin()), stl::RetrieveKey());
            openMVG::graph::indexedGraph putativeGraph(set_ViewIds, getPairs(map_PutativesMatches));
            openMVG::graph::exportToGraphvizData(_sMatchesDir / constants::cGraphPutativeMatches, putativeGraph);
        }

        auto filter_ptr = std::make_unique<openMVG::matching_image_collection::ImageCollectionGeometricFilter>(&_sfm_data, regions_provider);
        if (!filter_ptr)
        {
            _log->error("ReconstructionImpl::computeMatches: Invalid Regions Provider or Sfm Data");
            return;
        }

        openMVG::system::Timer timer;
        const double d_distance_ratio = 0.6;
        openMVG::matching::PairWiseMatches map_GeometricMatches;
        filter_ptr->Robust_model_estimation(
            openMVG::matching_image_collection::GeometricFilter_FMatrix_AC(4.0, imax_iteration), map_PutativesMatches, false, d_distance_ratio, nullptr);
        map_GeometricMatches = filter_ptr->Get_geometric_matches();

        // Save geometric filtered matches
        if (!openMVG::matching::Save(map_GeometricMatches, _sMatchesDir / sGeometricMatchesFilename))
        {
            _log->error("ReconstructionImpl::computeMatches: "
                        "Cannot save computed matches in \"%\"",
                _sMatchesDir / sGeometricMatchesFilename);
            return;
        }
        _log->message("Task done in (s): ", std::to_string(timer.elapsed()));

        if (write_details)
        {
            // Export Adjacency matrix
            _log->message("Export Adjacency Matrix of the pairwise's geometric matches");

            openMVG::matching::PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(), map_GeometricMatches, _sMatchesDir / constants::cGeometricAdjacencyMatrix_svg);
            // Export view pair graph once geometric filter have been done
            std::set<openMVG::IndexT> set_ViewIds;
            std::transform(_sfm_data.GetViews().begin(), _sfm_data.GetViews().end(), std::inserter(set_ViewIds, set_ViewIds.begin()), stl::RetrieveKey());
            openMVG::graph::indexedGraph geometricGraph(set_ViewIds, getPairs(map_GeometricMatches));
            openMVG::graph::exportToGraphvizData(_sMatchesDir / constants::cGraphGeometricMatches, geometricGraph);
        }
    };

    void incrementalSfM()
    {
        _log->message("Sequential/Incremental reconstruction"
                      "\nPerform incremental SfM (Initial Pair Essential + Resection)");

        const openMVG::cameras::Intrinsic_Parameter_Type intrinsic_refinement_options = openMVG::cameras::StringTo_Intrinsic_Parameter_Type("ADJUST_ALL");
        if (intrinsic_refinement_options == static_cast<openMVG::cameras::Intrinsic_Parameter_Type>(0))
        {
            _log->error("ReconstructionImpl::incrementalSfM: "
                        "Invalid input for Bundle Adjusment Intrinsic parameter refinement option");
            return;
        }

        // Initialise the region_type from the image_describer
        std::unique_ptr<openMVG::features::Regions> regions_type = openMVG::features::Init_region_type_from_file(_sOutputDir / constants::cImage_describer);
        if (!regions_type)
        {
            _log->error("ReconstructionImpl::incrementalSfM: Invalid: \"%\" regions type file", _sOutputDir / constants::cImage_describer);
            return;
        }

        // Read the features
        std::shared_ptr<openMVG::sfm::Features_Provider> feats_provider = std::make_shared<openMVG::sfm::Features_Provider>();
        if (!feats_provider->load(_sfm_data, _sFeaturesDir, regions_type))
        {
            _log->error("ReconstructionImpl::incrementalSfM: Invalid features");
            return;
        }

        // Read the Matches
        auto matches_provider = std::make_shared<openMVG::sfm::Matches_Provider>();
        if (!matches_provider->load(_sfm_data, _sMatchesDir / constants::cMatches_f_bin))
        {
            _log->error("ReconstructionImpl::incrementalSfM: Invalid matches file \"%\"", _sMatchesDir / constants::cMatches_f_bin);
            return;
        }

        // Incremental SfM
        openMVG::system::Timer timer;
        openMVG::sfm::SequentialSfMReconstructionEngine sfmEngine(_sfm_data, _sOutputDir, _sOutputDir / constants::cSfMReconstructionReport_html);
        sfmEngine.setInitialPair(openMVG::Pair(0, 1));
        sfmEngine.SetFeaturesProvider(feats_provider.get());
        sfmEngine.SetMatchesProvider(matches_provider.get());
        sfmEngine.Set_Intrinsics_Refinement_Type(intrinsic_refinement_options);
        sfmEngine.SetUnknownCameraType(cameraModelToMVG(_i_User_camera_model));

        if (!sfmEngine.Process())
        {
            _log->error("ReconstructionImpl::incrementalSfM: Sequential Reconstruction Engine could not complete its process ");
            return;
        }

        _log->message("Total Ac-Sfm took (s): %", std::to_string(timer.elapsed()));

        if (write_details)
        {
            _log->message("...Generating SfM_Report.html");
            openMVG::sfm::Generate_SfM_Report(sfmEngine.Get_SfM_Data(), _sOutputDir / constants::cSfMReconstructionReport_html);
        }
        // Export to disk computed scene (data & visualizable results)
        _log->message("..Export SfM_Data to disk");
        if (!openMVG::sfm::Save(sfmEngine.Get_SfM_Data(), _sOutputDir / constants::cSfM_data_bin, openMVG::sfm::ESfM_Data(openMVG::sfm::ALL)) ||
            !openMVG::sfm::Save(sfmEngine.Get_SfM_Data(), _sOutputDir / constants::cCloudAndPoses_ply, openMVG::sfm::ESfM_Data(openMVG::sfm::ALL)))
        {
            _log->error("ReconstructionImpl::incrementalSfM: Cannot save results in \"%\"", _sOutputDir);
            return;
        }
        // Update sfm data
        _sfm_data = sfmEngine.Get_SfM_Data();
    };

    void computeStructureFromKnownPoses()
    {
        _log->message("ReconstructionImpl::computeStructureFromKnownPoses: Compute Structure from the provided poses");

        double dMax_reprojection_error = 4.0;
        auto sOutputFile = _sOutputDir / constants::cPointCloudStructureFromKnownPoses_ply;

        // Initialise the region_type from the image_describer
        std::unique_ptr<openMVG::features::Regions> regions_type = openMVG::features::Init_region_type_from_file(_sOutputDir / constants::cImage_describer);

        if (!regions_type)
        {
            _log->error("ReconstructionImpl::computeStructureFromKnownPoses: "
                        "Invalid: \"%\" regions type file",
                _sOutputDir / constants::cImage_describer);
            return;
        }

        // Initialise the Regions provider
        std::shared_ptr<openMVG::sfm::Regions_Provider> regions_provider = std::make_shared<openMVG::sfm::Regions_Provider>();
        if (!regions_provider->load(_sfm_data, _sFeaturesDir, regions_type))
        {
            _log->error("ReconstructionImpl::computeStructureFromKnownPoses: Invalid regions");
            return;
        }

        _log->message("Loaded a sfm_data scene with:"
                      "\n    #views: %"
                      "\n    #poses: %"
                      "\n    #intrinsics: %"
                      "\n    #tracks: %",
            std::to_string(_sfm_data.GetViews().size()),
            std::to_string(_sfm_data.GetPoses().size()),
            std::to_string(_sfm_data.GetIntrinsics().size()),
            std::to_string(_sfm_data.GetLandmarks().size()));

        _log->message("============================================================="
                      "\nRobust triangulation of the tracks"
                      "\n - Triangulation of guided epipolar geometry matches"
                      "\n=============================================================");

        // Pair selection
        openMVG::Pair_Set pairs;
        if (!(_sMatchesDir / constants::cMatches_f_bin).empty())
        {
            openMVG::matching::PairWiseMatches matches;
            if (!openMVG::matching::Load(matches, _sMatchesDir / constants::cMatches_f_bin))
            {
                _log->error("ReconstructionImpl::computeStructureFromKnownPoses: "
                            "Unable to read the matches file \"%\"",
                    _sMatchesDir / constants::cMatches_f_bin);
                return;
            }
            pairs = getPairs(matches);
            // Keep only Pairs that belong to valid view indexes
            const std::set<openMVG::IndexT> valid_viewIdx = openMVG::sfm::Get_Valid_Views(_sfm_data);
            pairs = openMVG::sfm::Pair_filter(pairs, valid_viewIdx);
        }
        else
        {
            _log->error("ReconstructionImpl::computeStructureFromKnownPoses: "
                        "Cannot use match_file \"%\"",
                _sMatchesDir / constants::cMatches_f_bin);
            return;
        }

        openMVG::system::Timer timer;

        // Compute Structure from known camera poses
        openMVG::sfm::SfM_Data_Structure_Estimation_From_Known_Poses structure_estimator(dMax_reprojection_error);
        structure_estimator.run(_sfm_data, pairs, regions_provider);
        _log->message("Structure estimation took (s): %", std::to_string(timer.elapsed()));

        regions_provider.reset(); // Regions are not longer needed.
        openMVG::sfm::RemoveOutliers_AngleError(_sfm_data, 2.0);
        _log->message("#landmark found: ", std::to_string(_sfm_data.GetLandmarks().size()));

        if (write_details)
        {
            _log->message("...Generating SfM_Report.html");
            Generate_SfM_Report(_sfm_data, _sOutputDir / constants::cSfMStructureFromKnownPosesReport_html);
        }
        _log->message("Found a sfm_data scene with:"
                      "\n    #views: %"
                      "\n    #poses: %"
                      "\n    #intrinsics: %"
                      "\n    #tracks: %",
            std::to_string(_sfm_data.GetViews().size()),
            std::to_string(_sfm_data.GetPoses().size()),
            std::to_string(_sfm_data.GetIntrinsics().size()),
            std::to_string(_sfm_data.GetLandmarks().size()));

        // add extension if not present
        if (sOutputFile.extension() != ".ply")
        {
            sOutputFile += ".ply";
        }

        // Save the estimated cameras intrinsics, update the sfm_data and
        // save the resulting poincloud in the PLY format, write_in_ascii is set to true
        // the flag is set to STRUCTURE (does not output camera positions)
        if (!openMVG::sfm::Save_PLY(_sfm_data, sOutputFile, openMVG::sfm::ESfM_Data(openMVG::sfm::STRUCTURE), true /*write_in_ascii*/) ||
            !openMVG::sfm::Save(_sfm_data, _sOutputDir / constants::cIntrinsics, openMVG::sfm::ESfM_Data(openMVG::sfm::INTRINSICS)) ||
            !openMVG::sfm::Save(_sfm_data, _sOutputDir / constants::cSfM_data_bin, openMVG::sfm::ESfM_Data(openMVG::sfm::ALL)))
        {
            _log->error("ReconstructionImpl::computeStructureFromKnownPoses: "
                        "Cannot save results in: %",
                sOutputFile);
            return;
        }
    };

    void convertSfMStructure()
    {
        openMVG::sfm::ESfM_Data flag = openMVG::sfm::STRUCTURE;

        // Export the structure of the SfM_Data scene
        if (!Save(_sfm_data, _sOutputDir / constants::cMatches_json, flag))
        {
            _log->error("ReconstructionImpl::convertSfMStructure: "
                        "An error occured while trying to save \"%\"",
                _sOutputDir / constants::cMatches_json);
            return;
        }
    };
}; // end of class Reconstruction::Impl

Reconstruction::Reconstruction(AbstractLogger* log, std::filesystem::path p, CameraModel camera_model, bool write_details)
    : _impl(std::make_unique<Impl>(log, p, camera_model, write_details)){};

Reconstruction::~Reconstruction() = default;

void Reconstruction::sfmInitImageListing(double focal_pixels)
{
    assert(_impl);
    _impl->sfmInitImageListing(focal_pixels);
}

void Reconstruction::computeFeatures()
{
    assert(_impl);
    _impl->computeFeatures();
}

void Reconstruction::computeMatches()
{
    assert(_impl);
    _impl->computeMatches();
}

void Reconstruction::incrementalSfM()
{
    assert(_impl);
    _impl->incrementalSfM();
}

void Reconstruction::computeStructureFromKnownPoses()
{
    assert(_impl);
    _impl->computeStructureFromKnownPoses();
}

void Reconstruction::convertSfMStructure()
{
    assert(_impl);
    _impl->convertSfMStructure();
}

}; // namespace calimiro
