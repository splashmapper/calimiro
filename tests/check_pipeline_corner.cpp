/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <filesystem>
#include <memory>

#include "./calimiro/calimiro.h"

namespace fs = std::filesystem;

int main()
{
    const int position_count = 4;
    const int projector_count = 1;
    const double width = 1920.0;
    const double height = 1080.0;
    const double camera_focal = 2200.0;
    const double scale = 0.125;
    const auto data_path = fs::current_path() / "data" / "corner";

    calimiro::Workspace workspace(data_path);
    calimiro::Logger logger;
    calimiro::Structured_Light structured_light(&logger, scale);
    auto patterns = structured_light.create(width, height);
    auto capture_paths = workspace.generateStructuredLightCapturePaths(position_count, projector_count, patterns.size());

    auto capture_paths_it = capture_paths.cbegin();
    for (size_t position = 0; position < position_count; ++position)
    {
        std::vector<cv::Mat2i> decoded_projectors;
        for (size_t projector = 0; projector < projector_count; ++projector)
        {
            std::vector<std::string> image_paths;
            for (size_t i = 0; i < patterns.size(); ++i)
                image_paths.emplace_back(*(capture_paths_it++));

            auto images = workspace.readImagesFromList<cv::Mat1b>(image_paths);
            assert(images);

            auto decoded = structured_light.decode(width, height, images.value());
            assert(decoded);
            auto shadowmask = structured_light.getShadowMask();
            auto decoded_coordinates = structured_light.getDecodedCoordinates(width, height);
            assert(shadowmask && decoded_coordinates);

            calimiro::Workspace::ImageList images_to_save(
                {{"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_shadow_mask.jpg", shadowmask.value()},
                    {"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_x.jpg", decoded_coordinates.value().first},
                    {"decoded_images/pos_" + std::to_string(position) + "_proj" + std::to_string(projector) + "_y.jpg", decoded_coordinates.value().second}});
            assert(workspace.saveImagesFromList(images_to_save));

            decoded_projectors.push_back(decoded.value());
        }

        auto merged_projectors = workspace.combineDecodedProjectors(decoded_projectors);
        assert(merged_projectors);
        assert(workspace.exportMatrixToYaml(merged_projectors.value(), "decoded_matrix/pos_" + std::to_string(position)));
    }

    calimiro::Reconstruction reconstruction(&logger, data_path);
    reconstruction.sfmInitImageListing(camera_focal);
    reconstruction.computeFeatures();
    reconstruction.computeMatches();
    reconstruction.incrementalSfM();
    reconstruction.computeStructureFromKnownPoses();
    reconstruction.convertSfMStructure();

    // Generate the mesh
    auto points = calimiro::utils::readVerticesFromPly(
        &logger, std::filesystem::path(workspace.getWorkPath()) / calimiro::constants::cOutputDirectory / calimiro::constants::cPointCloudStructureFromKnownPoses_ply);
    auto geometry = calimiro::Geometry(&logger, points, {}, {}, {});
    auto geometry_normals = geometry.computeNormalsPointSet();
    auto geometry_mesh = geometry_normals.marchingCubes(600);
    auto geometry_mesh_clean = geometry_mesh.simplifyGeometry();

    // Values for eye selected from experimentation to get suitable texture coordinates
    const glm::vec3 eye_position(0.f, 0.f, 0.f);
    const glm::vec3 eye_direction(0.f, 0.f, 1.f);
    const float horizon_rotation = 0.f;
    const auto flip_horizontal = false;
    const auto flip_vertical = false;
    calimiro::TexCoordGenOrthographic generator(&logger, geometry_mesh_clean.vertices(), eye_position, eye_direction, horizon_rotation, flip_horizontal, flip_vertical);
    auto geometry_mesh_uv = geometry_mesh_clean.computeTextureCoordinates(&generator);

    calimiro::Obj obj(&logger, geometry_mesh_uv);
    obj.writeMesh(data_path / "final_mesh.obj");

    // Projector's calibration
    calimiro::MapXYZs map(&logger, data_path);
    map.pixelToProj(scale);
    std::map<int, std::vector<calimiro::CalibrationPoint>> matches_by_proj = map.sampling(15);
    std::vector<int> vec_inliers;
    std::vector<double> parameters;

    for (auto const& [proj, match_vector] : matches_by_proj)
    {
        vec_inliers.clear();
        parameters.clear();
        calimiro::Kernel kernel(&logger, std::make_shared<calimiro::cameramodel::Pinhole>(width, height), match_vector);
        parameters = kernel.Ransac(vec_inliers);
    }
}
