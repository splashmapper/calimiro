/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CALIMIRO_ESTIMATOR__
#define __CALIMIRO_ESTIMATOR__

#include <memory>
#include <vector>
#include <random>

#include "./abstract_logger.h"
#include "./calibration_point.h"
#include "./camera.h"
#include "./camera_model.h"
#include "./optimizer.h"

namespace calimiro
{
class Kernel
{
  public:
    using Model = std::vector<double>;
    using ModelVector = std::vector<Model>;
    static const int MINIMUM_SAMPLES = Camera::MINIMUM_SAMPLES;

    struct Container
    {
        Container(AbstractLogger* log_ptr, const std::shared_ptr<Camera>& camera, const std::vector<CalibrationPoint>& points)
            : log(log_ptr)
            , cam_ptr(camera)
            , pts(points){};

        AbstractLogger* log;
        std::shared_ptr<Camera> cam_ptr;
        std::vector<CalibrationPoint> pts;
    };

    /**
     * constructor
     */
    Kernel(AbstractLogger* log, std::shared_ptr<Camera> camera, const std::vector<CalibrationPoint>& xs)
        : _log(log)
        , _xs(xs)
        , _camera_ptr(camera)
        , _optimizer(std::make_unique<Nelder_Mead>(_log, _camera_ptr))
    {
    }

    /**
     * Returns the number of data points required by the minimal solver
     */
    inline int min_sample_size() const { return MINIMUM_SAMPLES; };

    /**
     * Returns the number of data points required by a non-minimal solver
     * We do not implement a non-minimal solver. But this is required by Ransac-Lib
     * to avoid generating samples of size 0 inside Ransac
     */
    inline int non_minimal_sample_size() const { return MINIMUM_SAMPLES; };

    /**
     * Returns the number of data points stored in the solver
     */
    size_t num_data() const { return _xs.size(); };

    /**
     * Fit models to the samples drawn by RANSAC
     * \param samples set of indices between 0 and num_data() -1
     * \param models estimated models
     * \return estimated number of models
     */
    size_t MinimalSolver(const std::vector<int>& samples, ModelVector* models) const;

    /**
     * This is not implemented at the moment
     * This function is called during local optimization to generate
     * a non-minimal sample from the inliers of the best model found so far
     */
    size_t NonMinimalSolver(const std::vector<int>&, Model*) const { return 0; };

    /**
     * Evaluates a given model on the i-th data point and returns the squared error
     * \param parameters parameters for the given model
     * \param id of the data point
     * \return squared error
     */
    double EvaluateModelOnPoint(const Model& parameters, int sample_id) const;

    /**
     * Performs least squares refinement of a given input model
     * This function is currently not implemented. Its implementation
     * would improve the effectiveness of the local optimization and RANSAC
     */
    void LeastSquares(const std::vector<int>&, Model*) const {};

    /**
     * Run RANSAC
     */
    Model Ransac(std::vector<int>& vec_inliers, float threshold = 0.3, int seed = std::random_device{}());

  private:
    AbstractLogger* _log;
    const std::vector<CalibrationPoint>& _xs;
    static const int _nb_extrinsic_params = 6;
    std::shared_ptr<Camera> _camera_ptr;
    std::unique_ptr<Nelder_Mead> _optimizer;
};
} // namespace calimiro
#endif // __CALIMIRO_ESTIMATOR__
