/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <memory>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/vector_angle.hpp>

#include "../calimiro/camera_model.h"
#include "../calimiro/estimator.h"
#include "../calimiro/logger.h"

using namespace calimiro;

/**
 * Verify the intrinsic and extrinsic camera parameters estimation. The set up is
 * based on the Blender scene in the file calimiro/tests/data/check_ransac.blend
 * The camera is at (0, -1, 0), looking at (0, 1, 0) with an up at (0, 0, 1).
 */
int main()
{
    std::vector<calimiro::CalibrationPoint> pts;

    // Points and their 2D correspondence for 2 intersecting planes
    // For the 2D points, the left corner is [0, camera_resolution.y]
    pts.push_back({glm::dvec3{-1.5152, 3.4848, 0.71429}, glm::dvec2{59.0, 964.0}});
    pts.push_back({glm::dvec3{-0.75761, 4.2424, 0.35714}, glm::dvec2{574.0, 723.0}});
    pts.push_back({glm::dvec3{-1.2627, 3.7373, -0.71429}, glm::dvec2{249.0, 139.0}});
    pts.push_back({glm::dvec3{2.0, 5.0, 1.0}, glm::dvec2{1848.0, 984.0}});
    pts.push_back({glm::dvec3{1.5, 5.0, -0.25}, glm::dvec2{1625.0, 429.0}});
    pts.push_back({glm::dvec3{1.75, 5.0, -1.0}, glm::dvec2{1736.0, 987.0}});
    pts.push_back({glm::dvec3{-0.50508, 4.4949, 1.0714}, glm::dvec2{714.0, 1061.0}});
    pts.push_back({glm::dvec3{-0.25254, 4.7475, -1.0714}, glm::dvec2{843.0, 43.0}});

    // 2 points with incorrect 2D projections. They have index 8 and 9 respectively
    pts.push_back({glm::dvec3{0.45609, 4.5439, 0.325}, glm::dvec2{0.0f, 0.0f}});
    pts.push_back({glm::dvec3{0.45609, 4.5439, -0.325}, glm::dvec2{540.0f, 1080.0f}});

    // Initialisation of the kernel
    double width = 1920.0;
    double height = 1080.0;
    Logger logger;
    cameramodel::Pinhole camera(width, height);
    Kernel kern(&logger, std::make_shared<cameramodel::Pinhole>(camera), pts);
    std::vector<int> vec_inliers;

    int seed = 2351996045;  // working random seed for RANSAC
    std::vector<double> param = kern.Ransac(vec_inliers, 0.5, seed);

    if (param.size() == 0)
    {
        std::cout << "\ncalimiro::tests::check_ransac - No camera parameters found" << std::endl;
        return 1;
    }
    // Verify that the incorrect points have not been added to the inliers vector of index
    assert((std::find(vec_inliers.begin(), vec_inliers.end(), 8) == vec_inliers.end()) == true);
    assert((std::find(vec_inliers.begin(), vec_inliers.end(), 9) == vec_inliers.end()) == true);

    // Camera has a veritcal fov of 22.90 degrees with cx = cy = 0.5
    // Verify that the estimation of the fov, cx and cy are reasonnable
    float fov_estimated = param[0];
    float cx_estimated = param[1];
    float cy_estimated = param[2];

    std::cout << "Estimated fov: " << fov_estimated << "\n"
              << "cx: " << cx_estimated << "\n"
              << "cy: " << cy_estimated << "\n";

    assert((fov_estimated > 19.0f && fov_estimated < 25.0f) == true);
    assert((cx_estimated > 0.4f && cx_estimated < 0.6f) == true);
    assert((cy_estimated > 0.4f && cy_estimated < 0.6f) == true);

    // Verify the extrinsic parameters (eye, target and up)
    int nb_param = param.size();
    glm::dvec3 estimated_eye{param[nb_param - 6], param[nb_param - 5], param[nb_param - 4]};
    glm::dvec3 estimated_euler{param[nb_param - 3], param[nb_param - 2], param[nb_param - 1]};
    glm::dvec3 true_eye{0.0, -1.0, 0.0};
    glm::dvec3 true_target{0.0, 1.0, 0.0};
    glm::dvec3 true_up{0.0, 0.0, 1.0};
    Camera::Extrinsics estimated_extrinsics = camera.computeExtrinsics(estimated_eye, estimated_euler);

    // Verify eye
    std::cout << "Eye: " << estimated_extrinsics.eye[0] << " " << estimated_extrinsics.eye[1] << " " << estimated_extrinsics.eye[2] << "\n";
    assert((glm::distance(estimated_extrinsics.eye, true_eye) < 0.25) == true);

    // Verify target
    // Target is a position -> there is multiple solutions along a line
    // To verify the target estimation, we make it a direction from the origin
    glm::dvec3 estimated_target_direction = glm::normalize(glm::dvec3(estimated_extrinsics.target - estimated_extrinsics.eye));
    std::cout << "Target direction: " << estimated_target_direction[0] << " " << estimated_target_direction[1] << " " << estimated_target_direction[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_target_direction, true_target, glm::cross(estimated_target_direction, true_target))) < 0.05) == true);

    // Verify up
    std::cout << "Up: " << estimated_extrinsics.up[0] << " " << estimated_extrinsics.up[1] << " " << estimated_extrinsics.up[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_extrinsics.up, true_up, glm::cross(estimated_extrinsics.up, true_up))) < 0.05) == true);

    return 0;
}
