/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CALIMIRO_REGION_FACTORY__
#define __CALIMIRO_REGION_FACTORY__

#include <openMVG/features/scalar_regions.hpp>

namespace openMVG
{
namespace features
{

// Define the Structured Light Keypoint
using STRUCTURED_LIGHT_Regions = Scalar_Regions<PointFeature, float, 3>; // region type could also be int, but openMVG regions_matcher does not support this option yet

} // namespace features
} // namespace openMVG
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION_INITIALIZER_LIST(openMVG::features::STRUCTURED_LIGHT_Regions);

// Register region type for serialization
#include <cereal/types/array.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/vector.hpp>
CEREAL_REGISTER_TYPE_WITH_NAME(openMVG::features::STRUCTURED_LIGHT_Regions, "STRUCTURED_LIGHT_Regions");
CEREAL_REGISTER_POLYMORPHIC_RELATION(openMVG::features::Regions, openMVG::features::STRUCTURED_LIGHT_Regions);

#endif // __CALIMIRO_REGION_FACTORY__
